const order = {
    toppings: [],
    basePrice: 80,
    total: 80
}

const toppings = [{
        name: 'pepperoni',
        price: 20
    },{
        name: 'mushroom',
        price: 40
    }, {
        name: 'cheese',
        price: 0
    }, {
        name: 'pineapple',
        price: 30
    }
]

// Cache topping containers
let $pizza  = null;
let $mushrooms = null;
let $pepperoni = null;
let $pineapple = null;

$(document).ready(function () {
    // Write the best jQuery ever here

    $pizza = $(".pizza");
    $mushrooms = $(".mushrooms");
    $pepperoni = $(".pepperonis");
    $pineapple = $(".pineapples");

    $("#cheese").click(function() {
        $pizza.toggleClass("no-cheese");
    })

    $("#mushroom").click(function() {
        if($mushrooms.children().length == 0) {
            $mushrooms.append(generateTopping(toppings[1]));
            order.total += toppings[1].price;
        } else {
            $mushrooms.html("");
            order.total -= toppings[1].price;
        }
        
        $("#total").html(order.total);
    })

    $("#pepperoni").click(function () {
        if ($pepperoni.children().length == 0) {
            $pepperoni.append(generateTopping(toppings[0]));
            order.total += toppings[0].price;
        } else {
            $pepperoni.html("");
            order.total -= toppings[0].price;
        }

        $("#total").html(order.total);
    })

    $("#pineapple").click(function () {
        if ($pineapple.children().length == 0) {
            $pineapple.append(generateTopping(toppings[3]));
            order.total += toppings[3].price;
        } else {
            $pineapple.html("");
            order.total -= toppings[3].price;
        }

        $("#total").html(order.total);
    })
});

// Automatically generate the toppings based on the name and id from the button.
function generateTopping(topping) {
    const looper = Array(10).fill(topping.name);
    return looper.map(item => `<div class="${item}"></div>`).join('');
}